﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReapAnalysis
{

    public enum QuarterOne
    {
        Jan = 01, Feb = 02, Mar = 03
    }
    public enum QuarterTwo
    {
        Apr = 04, May = 05, Jun = 06
    }
    public enum QuarterThree
    {
        Jul = 07, Aug = 08, Sep = 09
    }
    public enum QuarterFour
    {
        Oct = 10, Nov = 11, Dec = 12
    }

}
