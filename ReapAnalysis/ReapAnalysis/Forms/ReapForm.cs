﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using _Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Configuration;
using ClosedXML;

namespace ReapAnalysis
{

    public partial class ReapForm : Form
    {
        SqlDataReader dr;
        readonly string cs = ConfigurationManager.ConnectionStrings["ReapConn"].ConnectionString;//Main connection      
        public ReapForm()
        {
            InitializeComponent();
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                comboBox1.DataSource = GetAllTables(conn);
            }
            comboBox1.SelectedIndex = -1;
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            try 
            { 
                Export(comboBox1.SelectedValue.ToString());
            }
            catch(Exception)
            {
                MessageBox.Show("Error creating Excel report: No option selected" , "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }//end selectBtn_Click
        private string[] GetAllTables(SqlConnection connection)
        {
            List<string> result = new List<string>();
            SqlCommand cmd = new SqlCommand("SELECT name FROM sys.Tables where len(name) = 7 order by substring(name, 4, 4)", connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                result.Add(reader["name"].ToString());
            return result.ToArray();
        }//End GetAllTables


        private void Export(string tableName)
        { 
            string fileName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tableName + " Reap Analysis.xlsx";   

            _Excel.Application xlsApp;
            _Excel.Workbook xlsWorkbook;
            _Excel.Worksheet xlsWorksheetAnalysis;
            _Excel.Worksheet xlsWorksheetNot;
            _Excel.Worksheet xlsWorksheetPaidNotCan;
            _Excel.Worksheet xlsWorksheetPaidButCan;
            _Excel.Worksheet xlsWorksheetCodeEight;
            _Excel.Range textFormat;
            _Excel.Range sumFormat;
            object misValue = System.Reflection.Missing.Value;


            Cursor.Current = Cursors.WaitCursor;
            // Removes the old excel report file
            try
            {
                FileInfo oldFile = new FileInfo(fileName);
                if (oldFile.Exists)
                {
                    File.SetAttributes(oldFile.FullName, FileAttributes.Normal);
                    oldFile.Delete();
                }
            }//end try
            catch (Exception ex)
            {
                MessageBox.Show("Error removing old Excel report: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }//end catch

            try
            {
                xlsApp = new _Excel.Application();
                xlsWorkbook = xlsApp.Workbooks.Add();

                //////////////////////////////FORMATTING FOR CODE 8 SHEET///////////////////////////////////////////////////
                xlsWorksheetCodeEight = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetCodeEight.Name = "Code 8";

                _Excel.Range codeEightheaderRange = xlsWorksheetCodeEight.get_Range("A1", "B1");
                codeEightheaderRange.Borders.Color = Color.Black.ToArgb();
                codeEightheaderRange.Cells.Font.Bold = true;
                codeEightheaderRange.Interior.Color = Color.FromArgb(255, 255, 51);


                textFormat = xlsWorksheetCodeEight.get_Range("A:A, B:B");
                textFormat.NumberFormat = "@";
              
                //////////////////////////////FORMATTING FOR PAID BUT CANCELED SHEET///////////////////////////////////////////////////
                xlsWorksheetPaidButCan = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetPaidButCan.Name = "Centers PAID BUT Canceled";

                _Excel.Range paidButheaderRange = xlsWorksheetPaidButCan.get_Range("A1", "K1");
                paidButheaderRange.Borders.Color = Color.Black.ToArgb();
                paidButheaderRange.Cells.Font.Bold = true;
                paidButheaderRange.Interior.Color = Color.FromArgb(255, 255, 51);


                textFormat = xlsWorksheetPaidButCan.get_Range("A:A, B:B, C:C, E:E, F:F, G:G");
                textFormat.NumberFormat = "@";

                sumFormat = xlsWorksheetPaidButCan.get_Range("I:I");
                sumFormat.NumberFormat = "$ ##,###,###.00";

                //////////////////////////////FORMATTING FOR PAID NOT CANCELED SHEET///////////////////////////////////////////////////
                xlsWorksheetPaidNotCan = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetPaidNotCan.Name = "Centers PAID NOT Canceled";

                _Excel.Range notCanheaderRange = xlsWorksheetPaidNotCan.get_Range("A1", "K1");
                notCanheaderRange.Borders.Color = Color.Black.ToArgb();
                notCanheaderRange.Cells.Font.Bold = true;
                notCanheaderRange.Interior.Color = Color.FromArgb(255, 255, 51);


                textFormat = xlsWorksheetPaidNotCan.get_Range("A:A, B:B, C:C, E:E, F:F, G:G");
                textFormat.NumberFormat = "@";

                sumFormat = xlsWorksheetPaidNotCan.get_Range("I:I");
                sumFormat.NumberFormat = "$ ##,###,###.00";
                //////////////////////////////FORMATTING FOR NOT PAID SHEET///////////////////////////////////////////////////////////
                xlsWorksheetNot = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetNot.Name = "Centers Not Paid";


                _Excel.Range notHeaderRange = xlsWorksheetNot.get_Range("A1", "K1");
                notHeaderRange.Borders.Color = Color.Black.ToArgb();
                notHeaderRange.Cells.Font.Bold = true;
                notHeaderRange.Interior.Color = Color.FromArgb(255, 255, 51);


                textFormat = xlsWorksheetNot.get_Range("A:A, C:C, F:F, G:G, H:H");
                textFormat.NumberFormat = "@";

                
                //////////////////////////////////////////ANALYSIS FORMATTING////////////////////////////////////////////////////////
                xlsWorksheetAnalysis = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetAnalysis.Name = "Analysis";


                _Excel.Range headerRange = xlsWorksheetAnalysis.get_Range("A1", "C1");
                headerRange.Borders.Color = Color.Black.ToArgb();
                headerRange.Cells.Font.Bold = true;
                headerRange.Interior.Color = Color.FromArgb(255, 255, 51);

                _Excel.Range sumDepRange = xlsWorksheetAnalysis.get_Range("A2", "C2");
                sumDepRange.Cells.Font.Color = Color.FromArgb(204, 51, 51);
                sumFormat = xlsWorksheetAnalysis.get_Range("B2");
                sumFormat.NumberFormat = "$ ##,###,###.00";

                _Excel.Range rpdoCodeZeroRow = xlsWorksheetAnalysis.get_Range("A3", "C3");
                rpdoCodeZeroRow.Interior.Color = Color.FromArgb(102, 153, 102);

                _Excel.Range apCodeZeroRow = xlsWorksheetAnalysis.get_Range("A15", "C15");
                apCodeZeroRow.Interior.Color = Color.FromArgb(102, 153, 102);

                _Excel.Range returnCodeZero = xlsWorksheetAnalysis.get_Range("A27", "C27");
                returnCodeZero.Interior.Color = Color.FromArgb(102, 153, 102);

                //////////////////////////////////////////////END OF FORMATTING////////////////////////////////////////////////////////
                /////////////////////////////////////////////INSERTING/////////////////////////////////////////////////////////////////
                xlsWorkbook.Worksheets[6].Delete();//Deletes extra sheet that generates

                int i = 1;
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    conn.Open();

                    //CodeEightQuery
                    using (SqlCommand cmd = new SqlCommand(CodeEightQuery(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetCodeEight.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetCodeEight.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }
                        }
                        dr.Close();
                        codeEightheaderRange = xlsWorksheetCodeEight.get_Range("A:B");
                        codeEightheaderRange.Columns.AutoFit();
                    }//End CodeEightQuery cmd

                    i = 1;     
                    //PaidButQuery
                    using (SqlCommand cmd = new SqlCommand(PaidButQuery(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetPaidButCan.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetPaidButCan.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }
                        }
                        dr.Close();
                        paidButheaderRange = xlsWorksheetPaidButCan.get_Range("A:K");
                        paidButheaderRange.Columns.AutoFit();
                    }//End PaidButQuery cmd

                    //PaidNotCalQuery
                    i = 1;
                    using (SqlCommand cmd = new SqlCommand(PaidNotCanQuery(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetPaidNotCan.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetPaidNotCan.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }         
                        }
                        dr.Close();
                        notCanheaderRange = xlsWorksheetPaidNotCan.get_Range("A:K");
                        notCanheaderRange.Columns.AutoFit();
                    }//End PaidNotCalQuery cmd

                    //NotPaidQuery
                    i = 1;
                    using (SqlCommand cmd = new SqlCommand(NotPaidQuery(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetNot.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetNot.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }
                        }
                        dr.Close();
                        notHeaderRange = xlsWorksheetNot.get_Range("A:J");
                        notHeaderRange.Columns.AutoFit();
                    }//End NotPaidQuery cmd

                    //AnalysisQuery
                    i = 1;
                    using (SqlCommand cmd = new SqlCommand(AnalysisQuery(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetAnalysis.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetAnalysis.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }
                        }
                        dr.Close();
                        headerRange = xlsWorksheetAnalysis.get_Range("A:C");
                        headerRange.Columns.AutoFit();
                    }//End AnalysisQuery cmd


                    //Saving and closing out of processes
                    xlsWorkbook.SaveAs(fileName, _Excel.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue,
                              _Excel.XlSaveAsAccessMode.xlExclusive, _Excel.XlSaveConflictResolution.xlLocalSessionChanges, misValue, misValue, misValue, misValue);
                    xlsWorkbook.Close(true, misValue, misValue);
                    xlsApp.Quit();

                    ReleaseObject(xlsWorksheetAnalysis);
                    ReleaseObject(xlsWorksheetNot);
                    ReleaseObject(xlsWorksheetPaidButCan);
                    ReleaseObject(xlsWorksheetPaidNotCan);
                    ReleaseObject(xlsWorksheetCodeEight);
                    ReleaseObject(xlsWorkbook);
                    ReleaseObject(xlsApp);
                } //end SQLConnection

                Cursor.Current = Cursors.Default;
            }//end Try
                catch (Exception ex)
                {
                    MessageBox.Show("Error creating Excel report: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
        }//End Export
        static private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }//End ReleaseObject


        private string AnalysisQuery(string tableName)
        {
            string analysisReport = "select 'Sum to be deposited:'[Data], sum(AMOUNT)Amount, 'Deposit from UPS®'[Description] from " + tableName +
                               " union " +
                               "select 'RPDO Pkg Count code 0:'[Data], count(*)Amount, 'Good'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 0 " +
                               " union " +
                               "select 'RPDO Pkg Count code 1:'[Data], count(*)Amount, 'Dupe TN'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 1 " +
                               " union " +
                               "select 'RPDO Pkg Count code 2:'[Data], count(*)Amount, 'Previously Paid'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 2 " +
                               " union " +
                               "select 'RPDO Pkg Count code 3:'[Data], count(*)Amount, 'Invalid Scan'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 3 " +
                               " union " +
                               "select 'RPDO Pkg Count code 4:'[Data], count(*)Amount, 'Previously Delivered'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 4 " +
                               " union " +
                               "select 'RPDO Pkg Count code 5:'[Data], count(*)Amount, 'Invalid TN'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 5 " +
                               " union " +
                               "select 'RPDO Pkg Count code 6:'[Data], count(*)Amount, 'Access Point Pkg'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 6 " +
                               " union " +
                               "select 'RPDO Pkg Count code 7:'[Data], count(*)Amount, 'Manually Adjusted'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 7 " +
                               " union " +
                               "select 'RPDO Pkg Count code 8:'[Data], count(*)Amount, 'Non-Compensable Event'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 8 " +
                               " union " +
                               "select 'RPDO Pkg Count code 9:'[Data], count(*)Amount, 'Blackout Date Event'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 9 " +
                               " union " +
                               "select 'RPDO Pkg Count code 10:'[Data], count(*)Amount, 'Paid at Pickup Rate (CC ineligible)'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 10 " +
                               " union " +
                               "select 'RPDO Pkg Count code 11:'[Data], count(*)Amount, 'Paid at Drop Rate (CC ineligible)'[Description] from " + tableName + " where [INVOICE CODE] = 3 and[REASON CODE] = 11 " +
                               " union " +
                               "select 'AP Pkg Count code 0:'[Data], count(*)Amount, 'Good'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 0 " +
                               " union " +
                               "select 'AP Pkg Count code 1:'[Data], count(*)Amount, 'Dupe TN'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 1 " +
                               " union " +
                               "select 'AP Pkg Count code 2:'[Data], count(*)Amount, 'Previously Paid'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 2 " +
                               " union " +
                               "select 'AP Pkg Count code 3:'[Data], count(*)Amount, 'Invalid Scan'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 3 " +
                               " union " +
                               "select 'AP Pkg Count code 4:'[Data], count(*)Amount, 'Previously Delivered'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 4 " +
                               " union " +
                               "select 'AP Pkg Count code 5:'[Data], count(*)Amount, 'Invalid TN'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 5 " +
                               " union " +
                               "select 'AP Pkg Count code 6:'[Data], count(*)Amount, 'Access Point Pkg'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 6 " +
                               " union " +
                               "select 'AP Pkg Count code 7:'[Data], count(*)Amount, 'Manually Adjusted'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 7 " +
                               " union " +
                               "select 'AP Pkg Count code 8:'[Data], count(*)Amount, 'Non-Compensable Event'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 8 " +
                               " union " +
                               "select 'AP Pkg Count code 9:'[Data], count(*)Amount, 'Blackout Date Event'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 9 " +
                               " union " +
                               "select 'AP Pkg Count code 10:'[Data], count(*)Amount, 'Paid at Pickup Rate (CC ineligible)'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 10 " +
                               " union " +
                               "select 'AP Pkg Count code 11:'[Data], count(*)Amount, 'Paid at Drop Rate (CC ineligible)'[Description] from " + tableName + " where [INVOICE CODE] = 2 and[REASON CODE] = 11 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 0:'[Data], count(*)Amount, 'Good'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 0 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 1:'[Data], count(*)Amount, 'Dupe TN'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 1 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 2:'[Data], count(*)Amount, 'Previously Paid'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 2 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 3:'[Data], count(*)Amount, 'Invalid Scan'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 3 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 4:'[Data], count(*)Amount, 'Previously Delivered'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 4 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 5:'[Data], count(*)Amount, 'Invalid TN'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 5 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 6:'[Data], count(*)Amount, 'Access Point Pkg'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 6 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 7:'[Data], count(*)Amount, 'Manually Adjusted'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 7 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 8:'[Data], count(*)Amount, 'Non-Compensable Event'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 8 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 9:'[Data], count(*)Amount, 'Blackout Date Event'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 9 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 10:'[Data], count(*)Amount, 'Paid at Pickup Rate (CC ineligible)'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 10 " +
                               " union " +
                               "select 'Return to UPS Pkg Count code 11:'[Data], count(*)Amount, 'Paid at Drop Rate (CC ineligible)'[Description] from " + tableName + " where [INVOICE CODE] = 4 and[REASON CODE] = 11;";

            return analysisReport;
        }//End AnalysisQuery
        private string NotPaidQuery(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();

                SqlCommand getDate = new SqlCommand("SELECT TOP 1 CONVERT(VARCHAR(10), [INVOICE DATE], 1) FROM " + tableName + "", conn);
                string endDate = getDate.ExecuteScalar().ToString();

                SqlCommand getMonth = new SqlCommand("SELECT TOP 1 SUBSTRING(CONVERT(VARCHAR(10), [INVOICE DATE], 1), 1,2) FROM " + tableName + "", conn);
                string month = getMonth.ExecuteScalar().ToString();

                SqlCommand dropViewCreate = new SqlCommand("if exists(select 1 from sys.views where name='NotPaidView' and type='v') " +
                    "drop view NotPaidView;", conn);
                dropViewCreate.ExecuteNonQuery();
                SqlCommand viewCreate = new SqlCommand(
                    "create view NotPaidView as " +
                    "select y.*, IIF(z.CNUM is not null,'DUPE','')ISSUE  from " +
                    "(select x.*, v.DropoffCount, IIF(v.IsRpdo = 1, 'Yes', 'No')IsRpdo from " +
                    "(SELECT e.CNUM, e.CNAM[Name], e.SHP_ADD1[Address], e.SHP_CITY[City], e.SHP_ST[State], e.SHP_ZIP[ZipCode], replace(e.SHIPPER#,'-','')[CRIS], e.RPDODate " +
                    "FROM(SELECT a.CNUM, a.APID, a.CRIS, p.SHP_NAM, p.SHP_ADD1, p.SHP_CITY, p.SHP_ZIP, p.SHP_ST, a.Amount " +
                    "FROM(SELECT r.CNUM, m.APID, m.CRIS, SUM(m.AMOUNT) AS Amount " +
                    "FROM " + tableName + " AS m LEFT OUTER JOIN " +
                    "ReapUsers AS r ON m.APID + '01' = r.ReapID " +
                    "GROUP BY r.CNUM, m.APID, m.CRIS) AS a LEFT OUTER JOIN " +
                    "pecmast AS p ON a.CNUM = p.CNUM) AS b RIGHT OUTER JOIN " +
                    "(SELECT CNUM, CNAM, SHP_ADD1, SHP_CITY, SHP_ST, SHP_ZIP, SHIPPER#, ACCTNG, RPDODate " +
                    "FROM(SELECT CNUM, CNAM, SHP_ADD1, SHP_CITY, SHP_ST, SHP_ZIP, SHIPPER#, ACCTNG, CAST(REPLACE(RPDODate, '-', '/') AS date) AS RPDODate " +
                    "FROM(SELECT CNUM, CNAM, SHP_ADD1, SHP_CITY, SHP_ST, SHP_ZIP, SHIPPER#, ACCTNG, STUFF(ACCTNG, 1, PATINDEX('%[0-9/]%', ACCTNG) - 1, '') AS RPDODate " +
                    "FROM dbo.pecmast " +
                    "WHERE(ACCTNG LIKE '%rpdo%') AND(CRRTYPE NOT LIKE '%Z%')) AS c) AS d) AS e ON b.CNUM = e.CNUM " +
                    "where b.CNUM is null and e.RPDODate < '" + endDate + "')x " +
                    "left outer join PackageVolume as v on x.CNUM = v.CenterNumber " +
                    "where v.ReportYear = " + tableName.Substring(3, 4) + " and v.ReportMonth = " + month + ")y left outer join " +
                    "(select g.*, s.CNUM, s.[UPSShipper Number][CRIS] from " +
                    "(select h.ReapID from " +
                    "(select ReapID, count(ReapID)MyCount from ReapUsers " +
                    "group by ReapID)h " +
                    "where h.MyCount > 1)g left outer join " +
                    "ReapUsers as s on g.ReapID = s.ReapID)z on y.CNUM = z.CNUM;", conn);
                viewCreate.ExecuteNonQuery();

                string notReport = "select ReapId as UID, NotPaidView.* from NotPaidView " +
                    "join ReapUsers on ReapUsers.CNUM = NotPaidView.CNUM " +
                    "order by DropoffCount desc";
                return notReport;
            }
        }//End NotPaidQuery
        private string PaidNotCanQuery(string tableName)
        {
            string paidNotReport = "select b.*, IIF(y.ReapID is not null, 'DUPE', '')[IsDupe] from " +
                "(SELECT a.CNUM, a.APID, a.CRIS, p.SHP_NAM, p.SHP_ADD1, p.SHP_CITY, p.SHP_ZIP, p.SHP_ST, a.Amount[UPS® Amount], p.CRRTYPE " +
                "FROM(SELECT r.CNUM, m.APID, m.CRIS, SUM(m.AMOUNT) AS Amount " +
                "FROM " + tableName + " AS m LEFT OUTER JOIN " +
                "ReapUsers AS r ON m.APID + '01' = r.ReapID " +
                "GROUP BY r.CNUM, m.APID, m.CRIS) AS a LEFT OUTER JOIN " +
                "pecmast AS p ON a.CNUM = p.CNUM)b Left outer join " +
                "(select x.ReapID from (select ReapID, count(ReapID) MyCount from ReapUsers " +
                "group by ReapID)x " +
                "where x.MyCount > 1)y on b.APID + '01' = y.ReapID " +
                "where CRRTYPE not like '%Z%' " +
                "order by b.[UPS® Amount] desc;";

            return paidNotReport;
        }//End PaidNotCanQuery
        private string PaidButQuery(string tableName)
        {
            string paidButReport = "select b.*, IIF(y.ReapID is not null, 'DUPE', '')[IsDupe] from " +
               "(SELECT a.CNUM, a.APID, a.CRIS, p.SHP_NAM, p.SHP_ADD1, p.SHP_CITY, p.SHP_ZIP, p.SHP_ST, a.Amount[UPS® Amount], p.CRRTYPE " +
               "FROM(SELECT r.CNUM, m.APID, m.CRIS, SUM(m.AMOUNT) AS Amount " +
               "FROM " + tableName + " AS m LEFT OUTER JOIN " +
               "ReapUsers AS r ON m.APID + '01' = r.ReapID " +
               "GROUP BY r.CNUM, m.APID, m.CRIS) AS a LEFT OUTER JOIN " +
               "pecmast AS p ON a.CNUM = p.CNUM)b Left outer join " +
               "(select x.ReapID from " +
               "(select ReapID, count(ReapID) MyCount from ReapUsers " +
               "group by ReapID)x " +
               "where x.MyCount > 1)y on b.APID + '01' = y.ReapID " +
               "where CRRTYPE like '%Z%' " +
               "order by b.[UPS® Amount] desc;";

            return paidButReport;
        }//End PaidButQuery
        private string CodeEightQuery(string tableName)
        {
            string codeEightReport = "select cnum AS CNUM, count([Reason Code]) as 'Code 8' from " + tableName + " where[Reason Code] = 8 and[INVOICE CODE] = 3 group by cnum order by count([Reason Code]) desc";
            return codeEightReport;
        }//End CodeEightQuery




    }//end ReapForm   
}//end namespace