﻿
namespace ReapAnalysis
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.importBtn = new System.Windows.Forms.Button();
            this.rEAPDataSet = new ReapAnalysis.REAPDataSet();
            this.testTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testTableTableAdapter = new ReapAnalysis.REAPDataSetTableAdapters.TestTableTableAdapter();
            this.reapAnalysisbtn = new System.Windows.Forms.Button();
            this.quarterReapBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rEAPDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // importBtn
            // 
            this.importBtn.Location = new System.Drawing.Point(150, 26);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(177, 102);
            this.importBtn.TabIndex = 0;
            this.importBtn.Text = "Import Data";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // rEAPDataSet
            // 
            this.rEAPDataSet.DataSetName = "REAPDataSet";
            this.rEAPDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // testTableBindingSource
            // 
            this.testTableBindingSource.DataMember = "TestTable";
            this.testTableBindingSource.DataSource = this.rEAPDataSet;
            // 
            // testTableTableAdapter
            // 
            this.testTableTableAdapter.ClearBeforeFill = true;
            // 
            // reapAnalysisbtn
            // 
            this.reapAnalysisbtn.Location = new System.Drawing.Point(59, 134);
            this.reapAnalysisbtn.Name = "reapAnalysisbtn";
            this.reapAnalysisbtn.Size = new System.Drawing.Size(173, 104);
            this.reapAnalysisbtn.TabIndex = 1;
            this.reapAnalysisbtn.Text = "Monthly Reap Analysis";
            this.reapAnalysisbtn.UseVisualStyleBackColor = true;
            this.reapAnalysisbtn.Click += new System.EventHandler(this.reapAnalysisbtn_Click);
            // 
            // quarterReapBtn
            // 
            this.quarterReapBtn.Location = new System.Drawing.Point(249, 134);
            this.quarterReapBtn.Name = "quarterReapBtn";
            this.quarterReapBtn.Size = new System.Drawing.Size(173, 104);
            this.quarterReapBtn.TabIndex = 2;
            this.quarterReapBtn.Text = "Quarterly Reap Analysis";
            this.quarterReapBtn.UseVisualStyleBackColor = true;
            this.quarterReapBtn.Click += new System.EventHandler(this.quarterReapBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 283);
            this.Controls.Add(this.quarterReapBtn);
            this.Controls.Add(this.reapAnalysisbtn);
            this.Controls.Add(this.importBtn);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reap Analysis";
            ((System.ComponentModel.ISupportInitialize)(this.rEAPDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testTableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button importBtn;
        private REAPDataSet rEAPDataSet;
        private System.Windows.Forms.BindingSource testTableBindingSource;
        private REAPDataSetTableAdapters.TestTableTableAdapter testTableTableAdapter;
        private System.Windows.Forms.Button reapAnalysisbtn;
        private System.Windows.Forms.Button quarterReapBtn;
    }
}

