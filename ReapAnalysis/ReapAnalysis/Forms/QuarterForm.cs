﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using _Excel = Microsoft.Office.Interop.Excel;

namespace ReapAnalysis
{
    public partial class QuarterForm : Form
    {
        SqlDataReader dr;
        readonly string cs = ConfigurationManager.ConnectionStrings["ReapConn"].ConnectionString;//Main connection
        public QuarterForm()
        {
            InitializeComponent();
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                comboBox1.DataSource = GetAllTables(conn);
            }
            comboBox1.SelectedIndex = -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Export(comboBox1.SelectedValue.ToString());
            }
            catch (Exception)
            {
                MessageBox.Show("Error creating Excel report: No option selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }//End button1_Click
        private string[] GetAllTables(SqlConnection connection)
        {
            List<string> result = new List<string>();
            SqlCommand cmd = new SqlCommand("SELECT name FROM sys.Tables where len(name) = 13 order by name", connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                result.Add(reader["name"].ToString());
            return result.ToArray();
        }//End GetAllTables


        private void Export(string tableName)
        {
            string fileName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReapRaw" + tableName.Substring(7, 4) + tableName.Substring(11, 2) + " PAID AND NOT PAID.xlsx";

            _Excel.Application xlsApp;
            _Excel.Workbook xlsWorkbook;
            _Excel.Worksheet xlsWorksheetPaidFullQ;
            _Excel.Worksheet xlsWorksheetNotPaidFullQ;
            _Excel.Range textFormat;
            _Excel.Range sumFormat;
            object misValue = System.Reflection.Missing.Value;



            Cursor.Current = Cursors.WaitCursor;
            // Removes the old excel report file
            try
            {
                FileInfo oldFile = new FileInfo(fileName);
                if (oldFile.Exists)
                {
                    File.SetAttributes(oldFile.FullName, FileAttributes.Normal);
                    oldFile.Delete();
                }
            }//end try
            catch (Exception ex)
            {
                MessageBox.Show("Error removing old Excel report: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }//end catch

            try
            {
                xlsApp = new _Excel.Application();
                xlsWorkbook = xlsApp.Workbooks.Add();

                //////////////////////////////FORMATTING FOR CODE 8 SHEET///////////////////////////////////////////////////
               

                //////////////////////////////FORMATTING FOR PAID BUT CANCELED SHEET///////////////////////////////////////////////////
                xlsWorksheetNotPaidFullQ = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetNotPaidFullQ.Name = "NOT PAID";

                _Excel.Range notPaidFullheaderRange = xlsWorksheetNotPaidFullQ.get_Range("A1", "Q1");
                notPaidFullheaderRange.Borders.Color = Color.Black.ToArgb();
                notPaidFullheaderRange.Cells.Font.Bold = true;
                notPaidFullheaderRange.Interior.Color = Color.FromArgb(255, 255, 51);


                textFormat = xlsWorksheetNotPaidFullQ.get_Range("A:A, B:B, C:C, E:E, F:F, G:G");
                textFormat.NumberFormat = "@";

                sumFormat = xlsWorksheetNotPaidFullQ.get_Range("I:I");
                sumFormat.NumberFormat = "$ ##,###,###.00";



                //////////////////////////////////////////////END OF FORMATTING////////////////////////////////////////////////////////

                xlsWorksheetPaidFullQ = (_Excel.Worksheet)xlsWorkbook.Worksheets.Add();
                xlsWorksheetPaidFullQ.Name = "PAID";

                _Excel.Range paidFullQheaderRange = xlsWorksheetPaidFullQ.get_Range("A1", "S1");
                paidFullQheaderRange.Borders.Color = Color.Black.ToArgb();
                paidFullQheaderRange.Cells.Font.Bold = true;
                paidFullQheaderRange.Interior.Color = Color.FromArgb(255, 255, 51);


                textFormat = xlsWorksheetPaidFullQ.get_Range("A:Q");
                textFormat.NumberFormat = "@";

                sumFormat = xlsWorksheetPaidFullQ.get_Range("R:S");
                sumFormat.NumberFormat = "$ ##,###,###.00";

                /////////////////////////////////////////////INSERTING/////////////////////////////////////////////////////////////////
                xlsWorkbook.Worksheets[3].Delete();//Deletes extra sheet that generates

                CreatePaymentTable(tableName);//Created Payment Table

                int i = 1;
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    conn.Open();

                    //NotPaidFullQ
                    using (SqlCommand cmd = new SqlCommand(NotPaidFullQ(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetNotPaidFullQ.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetNotPaidFullQ.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }
                        }
                        dr.Close();

                        xlsWorksheetNotPaidFullQ.Application.ActiveWindow.SplitRow = 1;
                        xlsWorksheetNotPaidFullQ.Application.ActiveWindow.FreezePanes = true;
                        // Now apply autofilter
                        _Excel.Range firstRow = (_Excel.Range)xlsWorksheetNotPaidFullQ.Rows[1];
                        firstRow.AutoFilter(1,
                                            Type.Missing,
                                            _Excel.XlAutoFilterOperator.xlAnd,
                                            Type.Missing,
                                            true);
                        notPaidFullheaderRange = xlsWorksheetNotPaidFullQ.get_Range("A:Q");
                        notPaidFullheaderRange.Columns.AutoFit();
                    }//End NotPaidFullQ cmd

                    i = 1;
                    //PaidFullQ
                    CreatePaymentTable(tableName);
                    using (SqlCommand cmd = new SqlCommand(PaidFullQ(tableName), conn))
                    {
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                for (int j = 0; j < dr.FieldCount; ++j)
                                {
                                    xlsWorksheetPaidFullQ.Cells[i, j + 1] = dr.GetName(j);
                                }
                                ++i;
                            }

                            while (dr.Read())
                            {
                                for (int j = 1; j <= dr.FieldCount; ++j)
                                    xlsWorksheetPaidFullQ.Cells[i, j] = dr.GetValue(j - 1);

                                ++i;
                            }
                            
                        }
                       
                        dr.Close();
                       

                        xlsWorksheetPaidFullQ.Application.ActiveWindow.SplitRow = 1;
                        xlsWorksheetPaidFullQ.Application.ActiveWindow.FreezePanes = true;
                        // Now apply autofilter
                        _Excel.Range firstRow = (_Excel.Range)xlsWorksheetPaidFullQ.Rows[1];
                        firstRow.AutoFilter(1,
                                            Type.Missing,
                                            _Excel.XlAutoFilterOperator.xlAnd,
                                            Type.Missing,
                                            true);
                        paidFullQheaderRange = xlsWorksheetPaidFullQ.get_Range("A:S");
                        paidFullQheaderRange.Columns.AutoFit();
                    }//End PaidFullQ cmd



                    //Saving and closing out of processes
                    xlsWorkbook.SaveAs(fileName, _Excel.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue,
                              _Excel.XlSaveAsAccessMode.xlExclusive, _Excel.XlSaveConflictResolution.xlLocalSessionChanges, misValue, misValue, misValue, misValue);
                    xlsWorkbook.Close(true, misValue, misValue);
                    xlsApp.Quit();

                    
                    ReleaseObject(xlsWorksheetPaidFullQ);
                    ReleaseObject(xlsWorkbook);
                    ReleaseObject(xlsApp);
                } //end SQLConnection

                Cursor.Current = Cursors.Default;
            }//end Try
            catch (NullReferenceException ex)
            {
                MessageBox.Show("Cannot create Payment table, make sure all three months are in the quarter. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating Excel report: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
           
        }//End Export
        static private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }//End ReleaseObject


        private string PaidFullQ(string tableName)
        {
            string queryReturn = "select * from [dbo].[vPayments" + tableName.Substring(7, 4) + tableName.Substring(11, 2) + "] order by [Pec Pay Amount] desc";
            return queryReturn;   
        }//End PaidFullQ
        private string NotPaidFullQ(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                //First Month of Quarter
                SqlCommand getFirstMonth = new SqlCommand("select top 1 format(InvoiceDate, 'MM/dd/yyyy') from " + tableName + " " +
                    "group by InvoiceDate " +
                    "order by InvoiceDate ", conn);
                string firstMonth = getFirstMonth.ExecuteScalar().ToString();

                string getNameFirst = firstMonth.Substring(0, 2);
                SqlCommand nameFirstMonth = new SqlCommand("Select SUBSTRING(DateName( month , DateAdd( month , " + getNameFirst + " , 0 ) - 1 ), 1, 3)", conn);
                string nameFirst = nameFirstMonth.ExecuteScalar().ToString();//Holds first month substring

                //Second Month of Quarter
                SqlCommand getSecondMonth = new SqlCommand("SELECT format(InvoiceDate, 'MM/dd/yyyy') " +
                    "FROM " + tableName + " " +
                    "group by InvoiceDate " +
                    "ORDER BY InvoiceDate " +
                    "OFFSET 1 ROWS " +
                    "FETCH NEXT 1 ROWS ONLY; ", conn);
                string secondMonth = getSecondMonth.ExecuteScalar().ToString();

                string getNameSecond = secondMonth.Substring(0, 2);
                SqlCommand nameSecondMonth = new SqlCommand("Select SUBSTRING(DateName( month , DateAdd( month , " + getNameSecond + " , 0 ) - 1 ), 1, 3)", conn);
                string nameSecond = nameSecondMonth.ExecuteScalar().ToString();//Holds second month substring

                //Third Month of Quarter
                SqlCommand getThirdMonth = new SqlCommand("SELECT format(InvoiceDate, 'MM/dd/yyyy') " +
                    "FROM " + tableName + " " +
                    "group by InvoiceDate " +
                    "ORDER BY InvoiceDate " +
                    "OFFSET 2 ROWS " +
                    "FETCH NEXT 2 ROWS ONLY; ", conn);
                string thirdMonth = getThirdMonth.ExecuteScalar().ToString();//Holds date MM/dd/yyyy format for the third month in quarter

                string getNameThird = thirdMonth.Substring(0, 2);
                SqlCommand nameThirdMonth = new SqlCommand("Select SUBSTRING(DateName( month , DateAdd( month , " + getNameThird + " , 0 ) - 1 ), 1, 3)", conn);
                string nameThird = nameThirdMonth.ExecuteScalar().ToString();//Holds third month substring to the third character


                string notPaidFullQ = "select x.*, Z.* from " +
                    "(SELECT trim(p.CNUM)CNUM, replace(p.SHIPPER#, '-','')CRIS, trim(p.cnam)Name, trim(p.SHP_ADD1)Address, trim(p.SHP_CITY)City, trim(p.SHP_ST)State, " +
                    "trim(p.PH#)Phone, trim(p.NOTES)Email, trim(p.ACCTNG)ACCTNG, " +
                    "format(p.STARTUP, 'MM/dd/yyyy')Startup, format(p.STOPPED, 'MM/dd/yyyy')Stopped, p.CRRTYPE " +
                    "FROM pecmast AS p LEFT OUTER JOIN " +
                    "vPayments" + tableName.Substring(7, 4) + tableName.Substring(11, 2) + " AS v ON p.CNUM = v.CNUM " +
                    "WHERE(p.STARTUP < '" + thirdMonth + "') AND(p.CRRTYPE not like '%z%') and v.CNUM is null and p.ACCTNG like '%RPDO%')x " +
                    "left outer join " +
                    "(select H.*, (H.[" + nameFirst + " RPDO] + H.[" + nameSecond + " RPDO] + H.[" + nameThird + " RPDO])[PEC " + tableName.Substring(11, 2) + " RPDO] from " +
                    "(select g.CNUM, sum(g.[" + nameFirst + " RPDO])[" + nameFirst + " RPDO], sum(g.[" + nameSecond + " RPDO])[" + nameSecond + " RPDO], sum(g.[" + nameThird + " RPDO])[" + nameThird + " RPDO] from " +
                    "(select CenterNumber[CNUM], sum(DropoffCount)[" + nameFirst + " RPDO], null[" + nameSecond + " RPDO], null[" + nameThird + " RPDO] from PackageVolume where ReportYear = " + tableName.Substring(7, 4) + " and ReportMonth = " + getNameFirst + " " +
                    "group by CenterNumber " +
                    "union " +
                    "select CenterNumber[CNUM], null[" + nameFirst + " RPDO], sum(DropoffCount)[" + nameSecond + " RPDO], null[" + nameThird + " RPDO] from PackageVolume where ReportYear = " + tableName.Substring(7, 4) + " and ReportMonth = " + getNameSecond + " " +
                    "group by CenterNumber " +
                    "union " +
                    "select CenterNumber[CNUM], null[" + nameFirst + " RPDO], null[" + nameSecond + " RPDO], sum(DropoffCount)[" + nameThird + " RPDO] from PackageVolume where ReportYear = " + tableName.Substring(7, 4) + " and ReportMonth = " + getNameThird + " " +
                    "group by CenterNumber)g " +
                    "group by CNUM)H)z on x.CNUM = z.CNUM " +
                    "order by z.[PEC " + tableName.Substring(11, 2) + " RPDO] desc";

                return notPaidFullQ;
            }
        }//End NotPaidFullQ
        private void CreatePaymentTable(string tableName)
        {
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    conn.Open();
                    //First Month of Quarter
                    SqlCommand getFirstMonth = new SqlCommand("select top 1 format(InvoiceDate, 'MM/dd/yyyy') from " + tableName + " " +
                        "group by InvoiceDate " +
                        "order by InvoiceDate ", conn);
                    string firstMonth = getFirstMonth.ExecuteScalar().ToString();

                    string getNameFirst = firstMonth.Substring(0, 2);
                    SqlCommand nameFirstMonth = new SqlCommand("Select SUBSTRING(DateName( month , DateAdd( month , " + getNameFirst + " , 0 ) - 1 ), 1, 3)", conn);
                    string nameFirst = nameFirstMonth.ExecuteScalar().ToString();//Holds first month substring

                    //Second Month of Quarter
                    SqlCommand getSecondMonth = new SqlCommand("SELECT format(InvoiceDate, 'MM/dd/yyyy') " +
                        "FROM " + tableName + " " +
                        "group by InvoiceDate " +
                        "ORDER BY InvoiceDate " +
                        "OFFSET 1 ROWS " +
                        "FETCH NEXT 1 ROWS ONLY; ", conn);
                    string secondMonth = getSecondMonth.ExecuteScalar().ToString();

                    string getNameSecond = secondMonth.Substring(0, 2);
                    SqlCommand nameSecondMonth = new SqlCommand("Select SUBSTRING(DateName( month , DateAdd( month , " + getNameSecond + " , 0 ) - 1 ), 1, 3)", conn);
                    string nameSecond = nameSecondMonth.ExecuteScalar().ToString();//Holds second month substring

                    //Third Month of Quarter
                    SqlCommand getThirdMonth = new SqlCommand("SELECT format(InvoiceDate, 'MM/dd/yyyy') " +
                        "FROM " + tableName + " " +
                        "group by InvoiceDate " +
                        "ORDER BY InvoiceDate " +
                        "OFFSET 2 ROWS " +
                        "FETCH NEXT 2 ROWS ONLY; ", conn);
                    string thirdMonth = getThirdMonth.ExecuteScalar().ToString();

                    string getNameThird = thirdMonth.Substring(0, 2);
                    SqlCommand nameThirdMonth = new SqlCommand("Select SUBSTRING(DateName( month , DateAdd( month , " + getNameThird + " , 0 ) - 1 ), 1, 3)", conn);
                    string nameThird = nameThirdMonth.ExecuteScalar().ToString();//Holds third month substring

                    SqlCommand dropView = new SqlCommand("DROP VIEW if exists [vPayments" + tableName.Substring(7, 4) + tableName.Substring(11, 2) + "]", conn);
                    dropView.ExecuteNonQuery();


                    string paidFullQ =
                            "CREATE VIEW [dbo].[vPayments" + tableName.Substring(7, 4) + tableName.Substring(11, 2) + "] " +
                            "AS " +
                            "select e.CNUM, p.CNAM[Name], p.SHP_CITY[City], p.SHP_ST[State], p.PH#[Phone], p.NOTES[Email], e.CRIS, e.APID, e." + nameFirst + "RPDO, e." + nameFirst + "AP, e." + nameSecond + "RPDO,  " +
                            "e." + nameSecond + "AP, e." + nameThird + "RPDO, e." + nameThird + "AP, e.[Total RPDO Pkgs], e.[Total AP Pkgs], " +
                            "e.[Total Pkgs], e.[UPS® Amount], (e." + nameFirst + "RPDOPecAmount + e." + nameSecond + "RPDOPecAmount + e." + nameThird + "RPDOPecAmount + e.TotalAPPecAmount)[Pec Pay Amount] " +
                            "from " +
                            "( " +
                            "select d.*, iif(d." + nameFirst + "RPDO > 30, d." + nameFirst + "RPDO * .65, iif(d." + nameFirst + "RPDO between 11 and 30, d." + nameFirst + "RPDO * .5, d." + nameFirst + "RPDO * .4))" + nameFirst + "RPDOPecAmount, " +
                            "iif(d." + nameSecond + "RPDO > 30, d." + nameSecond + "RPDO * .65, iif(d." + nameSecond + "RPDO between 11 and 30, d." + nameSecond + "RPDO * .5, d." + nameSecond + "RPDO * .4))" + nameSecond + "RPDOPecAmount, " +
                            "iif(d." + nameThird + "RPDO > 30, d." + nameThird + "RPDO * .65, iif(d." + nameThird + "RPDO between 11 and 30, d." + nameThird + "RPDO * .5, d." + nameThird + "RPDO * .4))" + nameThird + "RPDOPecAmount, " +
                            "((d.[Total AP Pkgs]) * .4)[TotalAPPecAmount] " +
                            "from " +
                            "( " +
                            "select c.*, (c." + nameFirst + "RPDO + c." + nameSecond + "RPDO + c." + nameThird + "RPDO)[Total RPDO Pkgs], (c." + nameFirst + "AP + c." + nameSecond + "AP + c." + nameThird + "AP)[Total AP Pkgs], " +
                            "(c." + nameFirst + "RPDO + c." + nameSecond + "RPDO + c." + nameThird + "RPDO + c." + nameFirst + "AP + c." + nameSecond + "AP + c." + nameThird + "AP)[Total Pkgs], " +
                            "(((c." + nameFirst + "RPDO + c." + nameSecond + "RPDO + c." + nameThird + "RPDO) * .8) + ((c." + nameFirst + "AP + c." + nameSecond + "AP + c." + nameThird + "AP) * .5))[UPS® Amount] " +
                            "from " +
                            "( " +
                            "select b.CNUM, b.CRIS, b.APID, iif(b." + nameFirst + "RPDO is null, 0, b." + nameFirst + "RPDO)" + nameFirst + "RPDO, iif(b." + nameFirst + "AP is null, 0, b." + nameFirst + "AP)" + nameFirst + "AP, " +
                            "iif(b." + nameSecond + "RPDO is null, 0, b." + nameSecond + "RPDO)" + nameSecond + "RPDO, iif(b." + nameSecond + "AP is null, 0, b." + nameSecond + "AP)" + nameSecond + "AP, " +
                            "iif(b." + nameThird + "RPDO is null, 0, b." + nameThird + "RPDO)" + nameThird + "RPDO, iif(b." + nameThird + "AP is null, 0, b." + nameThird + "AP)" + nameThird + "AP " +
                            "from " +
                            "( " +
                            "select a.CNUM, a.CRIS, a.APID, sum(a." + nameFirst + "RPDO)" + nameFirst + "RPDO, sum(a." + nameFirst + "AP)" + nameFirst + "AP, sum(a." + nameSecond + "RPDO)" + nameSecond + "RPDO, sum(a." + nameSecond + "AP)" + nameSecond + "AP, " +
                            "sum(a." + nameThird + "RPDO)" + nameThird + "RPDO, sum(a." + nameThird + "AP)" + nameThird + "AP from " +
                            "( " +
                            "select CNUM, CRIS, APID, count(TrackingNumber)" + nameFirst + "RPDO, NULL[" + nameFirst + "AP], null[" + nameSecond + "RPDO], null[" + nameSecond + "AP], null[" + nameThird + "RPDO], null[" + nameThird + "AP] " +
                            "from " + tableName + " " +
                            "where InvoiceDate = '" + firstMonth + "' " +
                            "and ServiceCode = 3 and TrackingNumber is not null and (CompensationCode is null or CompensationCode = 0) " +
                            "group by CNUM, CRIS, APID " +
                            "union " +
                            "select CNUM, CRIS, APID, Null[" + nameFirst + "RPDO], count(TrackingNumber)" + nameFirst + "AP, null[" + nameSecond + "RPDO], null[" + nameSecond + "AP], null[" + nameThird + "RPDO], null[" + nameThird + "AP] " +
                            "from " + tableName + " " +
                            "where InvoiceDate = '" + firstMonth + "' " +
                            "and ServiceCode <> 3 and TrackingNumber is not null and (CompensationCode is null or CompensationCode = 0) " +
                            "group by CNUM, CRIS, APID " +
                            "union " +
                            "select CNUM, CRIS, APID, null[" + nameFirst + "RPDO], NULL[" + nameFirst + "AP], count(TrackingNumber)" + nameSecond + "RPDO, null[" + nameSecond + "AP], null[" + nameThird + "RPDO], null[" + nameThird + "AP] " +
                            "from " + tableName + " " +
                            "where InvoiceDate = '" + secondMonth + "' " +
                            "and ServiceCode = 3 and TrackingNumber is not null and (CompensationCode is null or CompensationCode = 0) " +
                            "group by CNUM, CRIS, APID " +
                            "union " +
                            "select CNUM, CRIS, APID, Null[" + nameFirst + "RPDO], null[" + nameFirst + "AP], null[" + nameSecond + "RPDO], count(TrackingNumber)" + nameSecond + "AP, null[" + nameThird + "RPDO], null[" + nameThird + "AP] " +
                            "from " + tableName + " " +
                            "where InvoiceDate = '" + secondMonth + "' " +
                            "and ServiceCode <> 3 and TrackingNumber is not null and (CompensationCode is null or CompensationCode = 0) " +
                            "group by CNUM, CRIS, APID " +
                            "union " +
                            "select CNUM, CRIS, APID, null[" + nameFirst + "RPDO], NULL[" + nameFirst + "AP], null[" + nameSecond + "RPDO], null[" + nameSecond + "AP], count(TrackingNumber)[" + nameThird + "RPDO], null[" + nameThird + "AP] " +
                            "from " + tableName + " " +
                            "where InvoiceDate = '" + thirdMonth + "' " +
                            "and ServiceCode = 3 and TrackingNumber is not null and (CompensationCode is null or CompensationCode = 0) " +
                            "group by CNUM, CRIS, APID " +
                            "union " +
                            "select CNUM, CRIS, APID, Null[" + nameFirst + "RPDO], null[" + nameFirst + "AP], null[" + nameSecond + "RPDO], null[" + nameSecond + "AP], null[" + nameThird + "RPDO], count(TrackingNumber)[" + nameThird + "AP] " +
                            "from " + tableName + " " +
                            "where InvoiceDate = '" + thirdMonth + "' " +
                            "and ServiceCode<> 3 and TrackingNumber is not null and (CompensationCode is null or CompensationCode = 0) " +
                            "group by CNUM, CRIS, APID " +
                            ")a " +
                            "group by a.CNUM, a.CRIS, a.APID " +
                            ")b " +
                            ")c " +
                            ")d " +
                            ")e " +
                            "left outer join " +
                            "pecmast as p on e.CNUM = p.CNUM ";
                    SqlCommand run = new SqlCommand(paidFullQ, conn);
                    run.ExecuteNonQuery();
                }  
        }//End CreatePaymentTable

    }//End Form
}//end NameSpace
