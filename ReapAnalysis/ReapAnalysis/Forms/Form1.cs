﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;
using System.Data.OleDb;
using ClosedXML.Excel;

namespace ReapAnalysis
{
    public partial class Form1 : Form
    {
        readonly string cs = ConfigurationManager.ConnectionStrings["ReapConn"].ConnectionString;//Main connection
        readonly string src = ConfigurationManager.ConnectionStrings["PECReapConn"].ConnectionString;//Connection to PECSQL2
        public Form1()
        {
            InitializeComponent(); 
        }


        private void reapAnalysisbtn_Click(object sender, EventArgs e)
        {
            ReapForm f2 = new ReapForm();
            f2.Show();
        }//End reapAnalysisbtn_Click
        private void quarterReapBtn_Click(object sender, EventArgs e)
        {
            QuarterForm f3 = new QuarterForm();
            f3.Show();
        }//End quarterReapBtn_Click
        private void importBtn_Click(object sender, EventArgs e)
        {
            try { 
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    conn.Open();
                    using (OpenFileDialog file = new OpenFileDialog())
                    {
                        file.DefaultExt = ".accdb";
                        file.Filter = "Access Database (*.accdb)|*.accdb";

                            if (file.ShowDialog() == DialogResult.OK)
                            {
                                string nameNoExt = Path.GetFileNameWithoutExtension(file.FileName);
                                var tableExists = new SqlCommand(@"select case when exists((select * from information_schema.tables where table_name = '" + nameNoExt + "')) then 1 else 0 end", conn);
                                if ((int)tableExists.ExecuteScalar() == 1)
                                {
                                    string message = "Table already found, do you wish to remake it?";
                                    string caption = "Table Found!";
                                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                                    DialogResult result;

                                    result = MessageBox.Show(message, caption, buttons);
                                    if (result == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        ImportFunction(file.FileName);
                                    }
                                    else if (result == System.Windows.Forms.DialogResult.No)
                                    {
                                        return;
                                    }
                                }//end if
                                else
                                {
                                    ImportFunction(file.FileName);
                                }//end else
                       
                       
                        }//end if ShowDialog 

                    }//end OpenFileDialog
                }//end SQLConnection
            }//end try
            catch (Exception ex)
            {
                MessageBox.Show("Error importing - make sure everything is spelled correctly: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }//end importBtn_Click


        private void ImportFunction(string file)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                string nameNoExt = Path.GetFileNameWithoutExtension(file);

                if (nameNoExt.Length == 7)
                {
                    if (Enum.IsDefined(typeof(QuarterOne), nameNoExt.Substring(0, 3)) || Enum.IsDefined(typeof(QuarterTwo), nameNoExt.Substring(0, 3)) || Enum.IsDefined(typeof(QuarterThree), nameNoExt.Substring(0, 3)) || Enum.IsDefined(typeof(QuarterFour), nameNoExt.Substring(0, 3)))
                    {
                        SqlTableCreate(nameNoExt);

                        Cursor.Current = Cursors.WaitCursor;
                        string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file + ";Persist Security Info=False;";

                        using (var sourceConnection = new OleDbConnection(connectionString))
                        {
                            sourceConnection.Open();

                            var commandSourceData = new OleDbCommand(
                                "SELECT " + nameNoExt + ".APID AS APID, " + nameNoExt + ".CRIS AS CRIS, " + nameNoExt + ".Country AS COUNTRY, " + nameNoExt + ".[Invoice Date] AS [INVOICE DATE], " + nameNoExt + ".[Contract] AS [CONTRACT], " + nameNoExt + ".[Invoice Type] AS [INVOICE TYPE], " + nameNoExt + ".Currency AS [CURRENCY], " + nameNoExt + ".[Invoice Code] AS [INVOICE CODE], " + nameNoExt + ".[Transaction Date] AS [TRANSACTION DATE], " + nameNoExt + ".[Tracking Number] AS [TRACKING NUMBER], " + nameNoExt + ".Amount AS [AMOUNT], " + nameNoExt + ".[Reason Code] AS [REASON CODE], " + nameNoExt + ".Reason AS REASON FROM " + nameNoExt + ";", sourceConnection);
                            var reader = commandSourceData.ExecuteReader();

                            using (var bulkCopy = new SqlBulkCopy(conn))
                            {
                                bulkCopy.DestinationTableName = "[dbo]." + nameNoExt;

                                try
                                {
                                    bulkCopy.WriteToServer(reader);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                finally
                                {
                                    reader.Close();
                                }
                            }

                            UpdateTable(nameNoExt);
                            CreateReapRaw(nameNoExt);

                            MessageBox.Show(CheckForNulls(nameNoExt));
                            MessageBox.Show("Import Successful!");
                        }//end OleDbConnection
                    }//end if enum check
                }//end if length check
            }//End sqlConnection
        }//end ImportFunction
        private void SqlTableCreate(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                var tableCreation = new SqlCommand("IF OBJECT_ID('[dbo].[" + tableName + "]', 'u') IS NOT NULL DROP TABLE [dbo].[" + tableName + "]; " +
                                             "CREATE TABLE [dbo].[" + tableName + "](" +
                                                 "[APID][nvarchar](255) NULL," +
                                                 "[CRIS][nvarchar](255) NULL," +
                                                 "[COUNTRY][nvarchar](255) NULL," +
                                                 "[INVOICE DATE][datetime] NULL," +
                                                 "[CONTRACT][nvarchar](255) NULL," +
                                                 "[INVOICE TYPE][nvarchar](255) NULL," +
                                                 "[CURRENCY][nvarchar](255) NULL," +
                                                 "[INVOICE CODE][float] NULL," +
                                                 "[TRANSACTION DATE][datetime] NULL," +
                                                 "[TRACKING NUMBER][nvarchar](255) NULL," +
                                                 "[AMOUNT][float] NULL," +
                                                 "[REASON CODE][float] NULL," +
                                                 "[REASON][nvarchar](255) NULL," +
                                                 "[CNUM][nvarchar](5) NULL )", conn);
                tableCreation.ExecuteNonQuery();
            }
        }//end SqlTableCreate
        private void UpdateTable(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                SqlCommand reasonCodeFix = new SqlCommand("UPDATE [dbo].[" + tableName + "] SET [REASON CODE] = '0' WHERE [REASON CODE] IS NULL;", conn);
                reasonCodeFix.ExecuteNonQuery();

                SqlCommand checkOnCrisAndShipperNum = new SqlCommand("UPDATE [dbo]." + tableName + " SET CNUM = p.CNUM FROM [dbo]." + tableName + " LEFT OUTER JOIN pecmast AS p ON [dbo]." + tableName + ".CRIS = replace(p.SHIPPER#, '-','')", conn);
                checkOnCrisAndShipperNum.ExecuteNonQuery();

                SqlCommand reapRawCheck = new SqlCommand("UPDATE [dbo]." + tableName + " SET CNUM = p.CNUM FROM [dbo]." + tableName + " LEFT OUTER JOIN ReapUsers AS p ON [dbo]." + tableName + ".CRIS = p.[UPSShipper Number] where [dbo]." + tableName + ".CNUM is null", conn);
                reapRawCheck.ExecuteNonQuery();

                SqlCommand checkOnAPIDAndReapID = new SqlCommand("UPDATE [dbo]." + tableName + " SET CNUM = p.CNUM FROM [dbo]." + tableName + " LEFT OUTER JOIN ReapUsers AS p ON [dbo]." + tableName + ".APID + '01' = p.ReapID where " + tableName + ".CNUM is null", conn);
                checkOnAPIDAndReapID.ExecuteNonQuery();
            }
        }
        private string CheckForNulls(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                string result = "";
                conn.Open();

                if (Enum.IsDefined(typeof(QuarterOne), tableName.Substring(0, 3)))
                {
                    SqlCommand nullCheck = new SqlCommand("select count(apid) from [dbo].ReapRaw" + tableName.Substring(3, 4) + "Q1 where cnum is null group by cnum", conn);
                    result = "Amount of Null CNUMS after Queries: " + Convert.ToString(nullCheck.ExecuteScalar()) + ". Please check ReapRaw table and make changes to both PECSQL2 and SQLExpress.";
                }

                else if (Enum.IsDefined(typeof(QuarterTwo), tableName.Substring(0, 3)))
                {
                    SqlCommand nullCheck = new SqlCommand("select count(apid) from [dbo].ReapRaw" + tableName.Substring(3, 4) + "Q2 where cnum is null group by cnum", conn);
                    result = "Amount of Null CNUMS after Queries: " + Convert.ToString(nullCheck.ExecuteScalar()) + ". Please check ReapRaw table and make changes to both PECSQL2 and SQLExpress."; 
                }

                else if (Enum.IsDefined(typeof(QuarterThree), tableName.Substring(0, 3)))
                {
                    SqlCommand nullCheck = new SqlCommand("select count(apid) from [dbo].ReapRaw" + tableName.Substring(3, 4) + "Q3 where cnum is null group by cnum", conn);
                    result = "Amount of Null CNUMS after Queries: " + Convert.ToString(nullCheck.ExecuteScalar()) + ". Please check ReapRaw table and make changes to both PECSQL2 and SQLExpress.";
                }

                else if (Enum.IsDefined(typeof(QuarterFour), tableName.Substring(0, 3)))
                {
                    SqlCommand nullCheck = new SqlCommand("select count(apid) from [dbo].ReapRaw" + tableName.Substring(3, 4) + "Q4 where cnum is null group by cnum", conn);
                    result = "Amount of Null CNUMS after Queries: " + Convert.ToString(nullCheck.ExecuteScalar()) + ". Please check ReapRaw table and make changes to both PECSQL2 and SQLExpress.";
                }
                return result;
            }
        }//end of CheckForNulls


        private void CreateReapRaw(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                if (Enum.IsDefined(typeof(QuarterOne), tableName.Substring(0, 3)))
                {
                    ReapQueries(tableName, "Q1");
                }

                else if (Enum.IsDefined(typeof(QuarterTwo), tableName.Substring(0, 3)))
                {
                    ReapQueries(tableName, "Q2");
                }

                else if (Enum.IsDefined(typeof(QuarterThree), tableName.Substring(0, 3)))
                {
                    ReapQueries(tableName, "Q3");
                }

                else if (Enum.IsDefined(typeof(QuarterFour), tableName.Substring(0, 3)))
                {
                    ReapQueries(tableName, "Q4");
                }     
            }//end using
        }//end CreateReapRaw
        private void ReapQueries(string tableName, string quarterNumber)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {  
                using (SqlConnection sqlconn = new SqlConnection(src))
                {   
                    conn.Open();
                    sqlconn.Open();
                    
                    if (String.Equals(tableName.Substring(0, 3), "Jan") || String.Equals(tableName.Substring(0, 3), "Apr") || String.Equals(tableName.Substring(0, 3), "Jul") || String.Equals(tableName.Substring(0, 3), "Oct"))
                    {
                        //Creates ReapRaw table inside of main connection
                        ReapRawTableCreate(tableName, quarterNumber, conn);
                        //Inserts from Month table to ReapRaw
                        ReapRawTransfer(tableName, quarterNumber, conn);

                        //Creates ReapRaw table inside of PECSQL2 
                        ReapRawTableCreate(tableName, quarterNumber, sqlconn);
                        PECSQLTransfer(tableName, quarterNumber);
                    }
                    else
                    {
                        //Runs just import if not the start of the quarter - I am assuming we will always get data from the month starting a quarter first
                        ReapRawTransfer(tableName, quarterNumber, conn);
                        PECSQLTransfer(tableName, quarterNumber);
                    }
                }//end SqlConnection sqlconn
            }//end SqlConnection conn
        }//end ReapQueries
        private void ReapRawTableCreate(string tableName, string quarterNumber, SqlConnection connection)
        {
            SqlCommand reapRawCreate = new SqlCommand("IF OBJECT_ID('[dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber + "', 'U') IS NOT NULL DROP TABLE [dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber + ";" +
                " IF NOT EXISTS (SELECT * FROM sys.objects " +
                           "WHERE object_id = OBJECT_ID(N'[dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber + "') AND type in (N'U'))" +
                           "BEGIN " +
                           "CREATE TABLE [dbo].[ReapRaw" + tableName.Substring(3, 4) + quarterNumber + "](" +
                           "[ID][int] IDENTITY(1, 1) NOT NULL," +
                           "[APID][varchar](9) NULL," +
                           "[CRIS][varchar](6) NULL," +
                           "[InvoiceDate][date] NULL," +
                           "[ServiceCode][int] NULL," +
                           "[TransactionDate][date] NULL," +
                           "[TrackingNumber][varchar](25) NULL," +
                           "[Amount][float] NULL," +
                           "[CompensationCode][int] NULL," +
                           "[Reason][varchar](50) NULL," +
                           "[CNUM][varchar](5) NULL," +
                       "CONSTRAINT[PK_dbo.ReapRaw" + tableName.Substring(3, 4) + quarterNumber + "] PRIMARY KEY CLUSTERED" +
                       "(" +
                           "[ID] ASC" +
                       ")WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY] " +
                       ") ON [PRIMARY]" +
                       "END", connection);
            reapRawCreate.ExecuteNonQuery();
           
        }//End of ReapRawTableCreate
        private void ReapRawTransfer(string tableName, string quarterNumber, SqlConnection connection)
        {
            SqlCommand reapTransfer = new SqlCommand("INSERT INTO [dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber + " (APID, CRIS, InvoiceDate, ServiceCode, TransactionDate, TrackingNumber, Amount, CompensationCode, Reason, CNUM) SELECT APID, CRIS, [INVOICE DATE], [INVOICE CODE], [TRANSACTION DATE], [TRACKING NUMBER], AMOUNT, [REASON CODE], REASON, CNUM FROM " + tableName, connection);
            reapTransfer.ExecuteNonQuery();
        }//End of ReapRawTransfer
        //Will create a datatable using the ReapRaw and export it to the PECSQL ReapRaw --needed because importing between servers takes too long
        private void PECSQLTransfer(string tableName, string quarterNumber)
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                using (SqlConnection sqlconn = new SqlConnection(src))
                {
                    sqlconn.Open();
                    SqlCommand refreshTable = new SqlCommand("truncate table [dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber, sqlconn);
                    refreshTable.ExecuteNonQuery();

                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber, conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);

                                using (var bulkCopy = new SqlBulkCopy(sqlconn.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                                    }

                                    bulkCopy.BulkCopyTimeout = 600;
                                    bulkCopy.DestinationTableName = "[dbo].ReapRaw" + tableName.Substring(3, 4) + quarterNumber;
                                    bulkCopy.WriteToServer(dt);
                                }//End bulkCopy
                            }//End DataTable dt
                        }//End SqlDataAdapter sda
                    }//End SqlCommand cmd
                }//End SqlConnection sqlconn
            }//End SqlConnection conn
        }//End PECSQLTransfer


    }//end class Form1
}//end namespace
