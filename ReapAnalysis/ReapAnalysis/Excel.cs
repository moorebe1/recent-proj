﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _Excel = Microsoft.Office.Interop.Excel;

namespace ReapAnalysis
{
    class Excel
    {
        string path = "";
        _Application excel = new _Excel.Application();
        Workbook wb;
        Worksheet ws;

        public Excel(string path, int sheet)
        {
            this.path = path;
            wb = excel.Workbooks.Open(path);
            ws = wb.Worksheets[sheet];
        }

        public bool ReadCell(int i, int j, string s)
        {
            
            i++;
            j++;
            if (ws.Cells[i, j].Value2 == s)
            {
                return false;
            }
            else
            {
                ws.Rows["1"].Insert();
                return true; 
            }
        }

        public void WriteToCell(int i, int j, string s)
        {     
            i++;
            j++;
            ws.Cells[i, j].Value2 = s;
        }

        public void Save()
        {
            wb.Save();
        }

        public void Close()
        {
            wb.Close();
        }


    }
}
