﻿
namespace UDVoice
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.summaryBtn = new System.Windows.Forms.Button();
            this.detailsBtn = new System.Windows.Forms.Button();
            this.allReportsBtn = new System.Windows.Forms.Button();
            this.importBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uDCallReportsDataSet = new UDVoice.UDCallReportsDataSet();
            this.uDCallDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uDCallDetailTableAdapter = new UDVoice.UDCallReportsDataSetTableAdapters.UDCallDetailTableAdapter();
            this.uDCallReportsDataSet1 = new UDVoice.UDCallReportsDataSet1();
            this.uDCallDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.uDCallDetailTableAdapter1 = new UDVoice.UDCallReportsDataSet1TableAdapters.UDCallDetailTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.extButton = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.mailboxBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mailBoxDataSet = new UDVoice.MailBoxDataSet();
            this.mailboxTableAdapter = new UDVoice.MailBoxDataSetTableAdapters.MailboxTableAdapter();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.extraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallReportsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallReportsDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallDetailBindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mailboxBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mailBoxDataSet)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // summaryBtn
            // 
            this.summaryBtn.Location = new System.Drawing.Point(14, 15);
            this.summaryBtn.Name = "summaryBtn";
            this.summaryBtn.Size = new System.Drawing.Size(120, 37);
            this.summaryBtn.TabIndex = 0;
            this.summaryBtn.Text = "Summary";
            this.summaryBtn.UseVisualStyleBackColor = true;
            this.summaryBtn.Click += new System.EventHandler(this.summaryBtn_Click);
            // 
            // detailsBtn
            // 
            this.detailsBtn.Location = new System.Drawing.Point(140, 15);
            this.detailsBtn.Name = "detailsBtn";
            this.detailsBtn.Size = new System.Drawing.Size(120, 37);
            this.detailsBtn.TabIndex = 1;
            this.detailsBtn.Text = "Details";
            this.detailsBtn.UseVisualStyleBackColor = true;
            this.detailsBtn.Click += new System.EventHandler(this.detailsBtn_Click);
            // 
            // allReportsBtn
            // 
            this.allReportsBtn.Location = new System.Drawing.Point(266, 15);
            this.allReportsBtn.Name = "allReportsBtn";
            this.allReportsBtn.Size = new System.Drawing.Size(120, 37);
            this.allReportsBtn.TabIndex = 2;
            this.allReportsBtn.Text = "Both Reports";
            this.allReportsBtn.UseVisualStyleBackColor = true;
            this.allReportsBtn.Click += new System.EventHandler(this.allReportsBtn_Click);
            // 
            // importBtn
            // 
            this.importBtn.Location = new System.Drawing.Point(14, 11);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(120, 37);
            this.importBtn.TabIndex = 5;
            this.importBtn.Text = "Import Data";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(281, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Reports By Person";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // uDCallReportsDataSet
            // 
            this.uDCallReportsDataSet.DataSetName = "UDCallReportsDataSet";
            this.uDCallReportsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uDCallDetailBindingSource
            // 
            this.uDCallDetailBindingSource.DataMember = "UDCallDetail";
            this.uDCallDetailBindingSource.DataSource = this.uDCallReportsDataSet;
            // 
            // uDCallDetailTableAdapter
            // 
            this.uDCallDetailTableAdapter.ClearBeforeFill = true;
            // 
            // uDCallReportsDataSet1
            // 
            this.uDCallReportsDataSet1.DataSetName = "UDCallReportsDataSet1";
            this.uDCallReportsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uDCallDetailBindingSource1
            // 
            this.uDCallDetailBindingSource1.DataMember = "UDCallDetail";
            this.uDCallDetailBindingSource1.DataSource = this.uDCallReportsDataSet1;
            // 
            // uDCallDetailTableAdapter1
            // 
            this.uDCallDetailTableAdapter1.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.detailsBtn);
            this.panel1.Controls.Add(this.summaryBtn);
            this.panel1.Controls.Add(this.allReportsBtn);
            this.panel1.Location = new System.Drawing.Point(50, 220);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 68);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.importBtn);
            this.panel2.Location = new System.Drawing.Point(77, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(152, 62);
            this.panel2.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(92, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Import CSV File";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Reports";
            // 
            // extButton
            // 
            this.extButton.Location = new System.Drawing.Point(307, 169);
            this.extButton.Name = "extButton";
            this.extButton.Size = new System.Drawing.Size(72, 23);
            this.extButton.TabIndex = 12;
            this.extButton.Text = "Search";
            this.extButton.UseVisualStyleBackColor = true;
            this.extButton.Click += new System.EventHandler(this.extButton_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.mailboxBindingSource;
            this.comboBox1.DisplayMember = "userDisplayName";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Location = new System.Drawing.Point(284, 142);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(117, 21);
            this.comboBox1.TabIndex = 13;
            this.comboBox1.ValueMember = "mailbox";
            // 
            // mailboxBindingSource
            // 
            this.mailboxBindingSource.DataMember = "Mailbox";
            this.mailboxBindingSource.DataSource = this.mailBoxDataSet;
            // 
            // mailBoxDataSet
            // 
            this.mailBoxDataSet.DataSetName = "MailBoxDataSet";
            this.mailBoxDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mailboxTableAdapter
            // 
            this.mailboxTableAdapter.ClearBeforeFill = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dateTimePicker1.Location = new System.Drawing.Point(155, 43);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(199, 20);
            this.dateTimePicker1.TabIndex = 15;
            this.dateTimePicker1.Value = new System.DateTime(2021, 8, 18, 0, 0, 0, 0);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(193, 69);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 37);
            this.button2.TabIndex = 16;
            this.button2.Text = "Get Reports By Date";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extraToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(507, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // extraToolStripMenuItem
            // 
            this.extraToolStripMenuItem.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.extraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addUserToolStripMenuItem});
            this.extraToolStripMenuItem.Name = "extraToolStripMenuItem";
            this.extraToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.extraToolStripMenuItem.Text = "Extra";
            // 
            // addUserToolStripMenuItem
            // 
            this.addUserToolStripMenuItem.Name = "addUserToolStripMenuItem";
            this.addUserToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.addUserToolStripMenuItem.Text = "Add User";
            this.addUserToolStripMenuItem.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(507, 308);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.extButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Unbound Digital Reports";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uDCallReportsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallReportsDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDCallDetailBindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mailboxBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mailBoxDataSet)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button summaryBtn;
        private System.Windows.Forms.Button detailsBtn;
        private System.Windows.Forms.Button allReportsBtn;
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.Label label1;
        private UDCallReportsDataSet uDCallReportsDataSet;
        private System.Windows.Forms.BindingSource uDCallDetailBindingSource;
        private UDCallReportsDataSetTableAdapters.UDCallDetailTableAdapter uDCallDetailTableAdapter;
        private UDCallReportsDataSet1 uDCallReportsDataSet1;
        private System.Windows.Forms.BindingSource uDCallDetailBindingSource1;
        private UDCallReportsDataSet1TableAdapters.UDCallDetailTableAdapter uDCallDetailTableAdapter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button extButton;
        private System.Windows.Forms.ComboBox comboBox1;
        private MailBoxDataSet mailBoxDataSet;
        private System.Windows.Forms.BindingSource mailboxBindingSource;
        private MailBoxDataSetTableAdapters.MailboxTableAdapter mailboxTableAdapter;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem extraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addUserToolStripMenuItem;
    }
}

