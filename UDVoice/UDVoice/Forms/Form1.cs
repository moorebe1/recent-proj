﻿using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web;
using System.Windows.Forms;
using UDVoice.Models;


namespace UDVoice
{
    public partial class Form1 : Form
    {
        readonly string cs = ConfigurationManager.ConnectionStrings["UDVoiceCon"].ConnectionString;
        public Form1()
        {
            InitializeComponent();
        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog file = new OpenFileDialog())
            {
                file.DefaultExt = ".csv";
                file.Filter = "Comma Separated (*.csv)|*.csv";
              

                if (file.ShowDialog() == DialogResult.OK)
                {
                    DataTable importData = GetDataFromFile(file.FileName);
                    if (importData == null) return;
                    SaveImportDataToDatabase(importData);
                    MessageBox.Show("Import Successful!");
                }
            }
        }
        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserForm f2 = new UserForm();
            f2.Show();
        }
        private void summaryBtn_Click(object sender, EventArgs e)
        {
            GetReport("CallSummary");
        }
        private void detailsBtn_Click(object sender, EventArgs e)
        {
            GetAll("CallDetails");
        }
        private void allReportsBtn_Click(object sender, EventArgs e)
        {
            GetReport("CallSummary");
            GetAll("CallDetails");
        }
        private void extButton_Click(object sender, EventArgs e)
        {
            GetReport("ExtReport");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ImportFromWeb();
        }
        private void Form1_Load(object sender, EventArgs e)
        { 
            // TODO: This line of code loads data into the 'mailBoxDataSet.Mailbox' table. You can move, or remove it, as needed.
            this.mailboxTableAdapter.Fill(this.mailBoxDataSet.Mailbox);
            DateTime dateTime = new DateTime();
            dateTime = DateTime.Now;
            switch (dateTime.DayOfWeek.ToString())
            {
                case "Sunday":
                    dateTime = dateTime.AddDays(-2);
                    break;
                case "Monday":
                    dateTime = dateTime.AddDays(-3);
                    break;
                case "Tuesday":
                case "Wednesday":
                case "Thursday":
                case "Friday":
                case "Saturday":
                    dateTime = dateTime.AddDays(-1);
                    break;
                default:
                    break;
            }

            dateTimePicker1.Text = dateTime.Date.ToString();

            comboBox1.SelectedIndex = -1;
        }


        //GENERATES REPORTS
        private void GetReport(string reportName)
        {
   
            if (comboBox1.SelectedIndex != -1 && reportName.Contains("ExtReport"))
            {
                using (SqlConnection conn = new SqlConnection(cs))
                {
                    conn.Open();
                    SqlCommand trunTable = new SqlCommand("TRUNCATE TABLE ExtTable", conn);
                    trunTable.ExecuteNonQuery();
                    SqlCommand getExt = new SqlCommand("INSERT INTO ExtTable SELECT CallType, CallDate, CallTime, Duration, Direction, FromNumber, FromName, " +
                                                     "FromDeviceType, ToNumber, ToName, ToDeviceType, UniqueCallID, GroupType " +
                                                     "FROM UDCallDetail cd " +
                                                     "JOIN Mailbox m on cd.fromname = m.userDisplayName " +
                                                     "OR cd.ToName = m.userDisplayName " +
                                                     "WHERE m.mailbox like '" + comboBox1.SelectedValue.ToString() + "'", conn);
                    getExt.ExecuteNonQuery();

                    ReportGenerate(reportName);

                    conn.Close();
                }
            }
            if(reportName.Contains("CallSummary"))
            {
                comboBox1.SelectedIndex = -1;
                ReportGenerate(reportName);
            }                       
        }
        //Gets All Reports From Combobox list will not save a pdf if there is nothing in report
        private void GetAll(string reportName)
        { 
            using (SqlConnection conn = new SqlConnection(cs))
            {
                //string sql = "SELECT * " +
                //             "FROM UDCallDetail";
                
                //comboBox1.SelectedIndex = 0;
                //for (int i = 1; i < comboBox1.Items.Count; i++)
                //{
                //    conn.Open();
                //    SqlCommand cmd = new SqlCommand(sql, conn);
                //    comboBox1.SelectedIndex = i;
                //    conn.Close();
                //}
                comboBox1.SelectedIndex = -1;
                ReportGenerate(reportName);
                conn.Close();
            }
        }


        //Takes data from CSV file and inserts into a DataTable
        private DataTable GetDataFromFile(string csv_file_path)
        {

            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            return csvData;
        }
        //Runs SQL to truncate previous data - import new data - and clean up said data
        private void SaveImportDataToDatabase(DataTable importedData)
        {
            
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                SqlCommand trunTable = new SqlCommand("dbo.Truncate_UDCallDetails", conn);
                trunTable.CommandType = CommandType.StoredProcedure;
                trunTable.ExecuteReader();
                

                using (SqlBulkCopy s = new SqlBulkCopy(conn))
                {
                    s.DestinationTableName = "UDCallDetail";
                    for (int i = 0; i < importedData.Columns.Count; i++)
                    {     
                        s.ColumnMappings.Add(i, i);
                    }
                    s.WriteToServer(importedData);
                }
                SQLDataFix();       
            }

        }
        


        private void ReportGenerate(string reportName)
        {
            ReportExecutionService rs = new ReportExecutionService();
            rs.Credentials = CredentialCache.DefaultCredentials;
            string server = ConfigurationManager.AppSettings["server"];
            rs.Url = "http://pecsql3:81/reportserver/reportexecution2005.asmx";
            rs.ExecutionHeaderValue = new ExecutionHeader();
            var excutionInfo = new ExecutionInfo();
            excutionInfo = rs.LoadReport("/UDCalls/" + reportName, null);
            List<ParameterValue> parameters = new List<ParameterValue>();
            string deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            string mimeType;
            string encoding;
            string[] streamId;
            Warning[] warning;
            var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "pdf";
            if(comboBox1.SelectedIndex != -1 && reportName.Contains("ExtReport"))
            {
                saveFileDialog.FileName = comboBox1.SelectedValue + reportName;
            }
            else
            {
                saveFileDialog.FileName = reportName;
            }
            
            saveFileDialog.ShowDialog();
            File.WriteAllBytes(saveFileDialog.FileName, result);
        }
        private IRestResponse CreateURL()
        {
            DateTime dateFormat;
            DateTime.TryParse(dateTimePicker1.Value.ToString(), out dateFormat);
            var formattedDate = dateFormat.ToString("yyyy-MM-dd");
            var nextDayFormated = dateFormat.AddDays(1).ToString("yyyy-MM-dd");


            string startOfDay = formattedDate + "T06:00:00.000Z";        
            string endOfDay = nextDayFormated + "T02:00:00.000Z";

            string longurl = "https://api.elevate.services//analytics/calls/user?";
            var uriBuilder = new UriBuilder(longurl);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["dateFrom"] = startOfDay;
            query["dateTo"] = endOfDay;
            uriBuilder.Query = query.ToString();
            longurl = uriBuilder.ToString();

            var executeURL = new RestClient(longurl);
            executeURL.Timeout = -1;
            var requestURL = new RestRequest(Method.POST);

            requestURL.AddHeader("Content-Type", "application/json");
            requestURL.AddHeader("Authorization", "Bearer " + GenerateToken() + "");
            requestURL.AddHeader("Cookie", "JSESSIONID=E13DDB25A9A3E5FC69B1036CE8DB99D4");
            IRestResponse responseURL = executeURL.Execute(requestURL);
            return responseURL;
        }
        private void ImportFromWeb()
        {
            try { 
            Cursor.Current = Cursors.WaitCursor;
            IList<CallDetail> items = new List<CallDetail>();
            dynamic responseContent = JsonConvert.DeserializeObject(CreateURL().Content);
            string input = responseContent["calls"].ToString();


            var output = JsonConvert.DeserializeObject<List<dynamic>>(input);
          
            foreach (dynamic call in output)
            {
                var itemList = new CallDetail();
                itemList.CallType = "Call";

                //Fixed time/date
                string dateEdit = call["start"].ToString().Substring(0, call["start"].ToString().IndexOf(" ", + 1));
                string timeEdit = call["start"].ToString().Remove(0, call["start"].ToString().IndexOf(' ') + 1);            

                itemList.CallDate = dateEdit;
                itemList.CallTime = timeEdit;
                itemList.Duration = call["duration"].ToString();
                itemList.Direction = call["direction"].ToString();
                itemList.FromNumber = call["from"]["number"].ToString();
                itemList.FromName = call["from"]["name"].ToString();
                itemList.FromDeviceType = call["to"]["device"]["deviceType"].ToString();
                itemList.ToNumber = call["to"]["number"].ToString();
                itemList.ToName = call["to"]["name"].ToString();
                itemList.ToDeviceType = call["to"]["device"]["deviceType"].ToString();
                itemList.UniqueCallID = call["id"].ToString();
                itemList.GroupType = "CC Group Ring All";
                itemList.GlobalCallID = call["globalCallId"].ToString();
                items.Add(itemList);
            }

            //Takes IList and converts it to Datatable for it to be imported to SQL table
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();

                SqlCommand trunTable = new SqlCommand("TRUNCATE TABLE UDCallDetail", conn);
                trunTable.ExecuteNonQuery();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(cs))
                {
                    bulkCopy.DestinationTableName = "dbo.UDCallDetail";
                    try
                    {
                        // Writes datatable to sql table
                        bulkCopy.WriteToServer(ToDataTables(items));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
               FixWebImportSql();
            }
            Cursor.Current = Cursors.Default;
            GetReport("CallSummary");
            GetAll("CallDetails");
            }
            catch(Exception e)
            {

            }
        }//end importFromWeb


        //Generates Token for API call
        private string GenerateToken()
        {

            var client = new RestClient("https://login.serverdata.net/user/connect/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", "grant_type=client_credentials&client_id=FjgT40e38EWgdW5dXiGGHQ&client_secret=RVlSkKG6BRU5WjzbsrClUn1QSOxp6mbFxVgVdPds6XE", ParameterType.RequestBody);
            request.AddHeader("authorization", "Bearer <access_token>");

            IRestResponse response = client.Execute(request);
            JObject obj = JObject.Parse(response.Content);
            var token = (JValue)obj.SelectToken("access_token");
            return token.ToString();
        }//End GenerateToken
        //Converts IList to DataTable
        private DataTable ToDataTables<T>(IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prp = props[i];
                table.Columns.Add(prp.Name, prp.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }//End ToDataTables
        private void FixWebImportSql()
        {
            DateTime dateFormat;
            DateTime.TryParse(dateTimePicker1.Value.ToString(), out dateFormat);
            var formattedDate = dateFormat.ToString("yyyy-MM-dd");

            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                SqlCommand SQLDataFormat = new SqlCommand("dbo.SQLWebDataFix_UDCallDetails", conn);
                SQLDataFormat.CommandType = CommandType.StoredProcedure;
                var param = new SqlParameter("@formattedDate", formattedDate);
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.String;
                SQLDataFormat.Parameters.Add(param);
                SQLDataFormat.ExecuteReader();
            }
        }//End FixWebImportSql
        private void SQLDataFix()
        {
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                SqlCommand SQLDataFormat = new SqlCommand("dbo.SQLDataFix_UDCallDetails", conn);
                SQLDataFormat.CommandType = CommandType.StoredProcedure;
                SQLDataFormat.ExecuteReader();
            }
        }//End SQLDataFix


    }//End Form



}//End namespace
