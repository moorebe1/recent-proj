﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace UDVoice
{
    public partial class UserForm : Form
    {
        readonly string cs = ConfigurationManager.ConnectionStrings["UDVoiceCon"].ConnectionString;
        ToolTip t1 = new ToolTip();
        public UserForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text.Length < 3)
            {
                MessageBox.Show("Extention must be at least 3 characters long.");
                return;
            }
            if (string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrWhiteSpace(textBox2.Text))
            {
                MessageBox.Show("Name textbox must not be empty.");
                return;
            }

            addUser(textBox1.Text, textBox2.Text, textBox3.Text);
            this.Close();
        }

        public void addUser(string ext, string name, string email)
        {

            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                SqlCommand adduserSQL = new SqlCommand("insert Mailbox(mailbox, userDisplayName, username) values('" + ext + "', '" + name + "', '" + email + "')", conn);
                adduserSQL.ExecuteNonQuery();
                MessageBox.Show("User Added!");
                conn.Close();
            }
        }


        private void textBox1_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox3_MouseHover(object sender, EventArgs e)
        {
            t1.SetToolTip(textBox3, "If no is email set up, use noemail@PackageExpress.com.");
        }

        private void textBox3_Enter(object sender, EventArgs e)
        {
            t1.Show("If no is email set up, use noemail@PackageExpress.com.", textBox3);
        }


    }
}
