﻿namespace UDVoice.Models
{
    public class CallDetail
    {
        
       
        public string CallType { get; set; }
        public string CallDate { get; set; }
        public string CallTime { get; set; }
        public string Duration { get; set; }
        public string Direction { get; set; }
        public string FromNumber { get; set; }
        public string FromName { get; set; }
        public string FromDeviceType { get; set; }
        public string ToNumber { get; set; }
        public string ToName { get; set; }
        public string ToDeviceType { get; set; }
        public string UniqueCallID { get; set; }
        public string GroupType { get; set; }
        public string GlobalCallID { get; set; }

    }
}
